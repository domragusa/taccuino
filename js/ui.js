import { storage } from './storage.js';
import { tpl, screenManager, editorWidget, listWidget, buttonFactory, headerFactory } from './ui-toolkit.js';

class ui{
    constructor(){
        let st = new storage();
        let screen_man = new screenManager();
        let editor = new editorWidget();
        let note_list = new listWidget(
            new tpl('#tpl_note_row'),
            (data)=>{
                console.log(data);
                editor_loader(data);
                screen_man.activate('editor');
            }
        );
        
        let cur_file = undefined;
        
        
        
        let back_btn = buttonFactory.makeFromName('back', ()=>{screen_man.activate('browser')});
        
        let editor_title = document.createElement('div');
        editor_title.contentEditable = true;
        
        let save_btn = buttonFactory.makeFromName('save', ()=>{
            let cur_title   = editor_title.textContent,
                cur_content = editor.store();
            if(!cur_file){ // new file
                st.addFile(cur_title, cur_content).then((id)=>{cur_file=id;});
                // BUG: cur_file is undefined while the promise is still 
                // pending if another save event is fired there'll be a
                // duplicate. Needs fixing.
            }else{
                st.writeFile(cur_file, cur_title, cur_content);
            }
            
        });
        let editor_header = headerFactory(back_btn, editor_title, [save_btn, buttonFactory.makeFromName('more')]);
        
        let editor_loader = (data) => {
            cur_file = data.key;
            let p = st.readFile(cur_file);
            p.then((res)=>{
                editor_title.textContent = res.title;
                editor.load(res.content);
            }).catch(alert);
        };
        let editor_screen = document.createElement('div');
        editor_screen.appendChild(editor_header);
        editor_screen.appendChild(editor.container);
        
        screen_man.registerScreen('editor', editor_screen, ()=>{console.log('showing editor')});
        

        
        let menu_btn = buttonFactory.makeFromName('menu', ()=>{console.log('not implemented!')});
        let header_browser = headerFactory(menu_btn, 'Taccuino');
        
        let new_button = buttonFactory.makeFromName('plus', ()=>{
            cur_file = undefined;
            editor_title.textContent = 'No title';
            editor.load('');
            screen_man.activate('editor');
        }, 'fab');
        
        let browser_screen = document.createElement('div');
        browser_screen.appendChild(header_browser);
        browser_screen.appendChild(note_list.container);
        browser_screen.appendChild(new_button);
        let browser_loader = () => {
            st.listFiles().then(
                (data)=>{
                    for(let i in data){
                        data[i].lastEdit = niceDate(data[i].lastEdit);
                    }
                    note_list.load(data);
                }
            ).catch(alert);
        };
        screen_man.registerScreen('browser', browser_screen, browser_loader);
        
        
        this.screen_man = screen_man;
        
    }
}

function niceDate(date){
    let months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
    let today = new Date();
    
    if(today.toDateString() == date.toDateString()){
        return 'today';
    }else if(date.toDateString() == (new Date(today-86400000)).toDateString()){
        return 'yesterday'
    }else{
        let tmp = date.getDate() + ' ' + months[date.getMonth()];
        if(date.getFullYear() != today.getFullYear()){
            tmp += ' ' + date.getFullYear();
        }
        return tmp;
    }
    
}





document.addEventListener('DOMContentLoaded', ()=>{
    let test = new ui();
    document.body.appendChild(test.screen_man.container);
    test.screen_man.activate('browser');
    
    /*st = new storage();
    app = new editorWidget(st);
    document.body.appendChild(app.toolbartext);
    document.body.appendChild(app.title);
    document.body.appendChild(app.editarea);*/
});
