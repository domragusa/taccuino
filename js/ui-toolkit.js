export class tpl{
    constructor(q){
        this.base = document.querySelector(q);
    }
    make(data){
        let tmp = this.base.cloneNode(true).content.querySelector('*');
        for(let q in data){
            let cur_val = data[q];
            let cur_tag = tmp.getElementsByClassName(q)[0];
            if(!cur_tag) continue;
            cur_tag.textContent = data[q];
        }
        return tmp;
    }
}

export class screenManager{
    constructor(){
        this.active = undefined;
        this.container = document.createElement('div');
        this.screens = {};
    }
    registerScreen(name, screen, callback){
        this.screens[name] = {container: screen, callback: callback};
        this.container.appendChild(screen);
        screen.style.display = 'none';
    }
    activate(name){
        if(!(name in this.screens)) return;
        let cur = this.screens[name];
        
        if(this.active)this.screens[this.active].container.style.display = 'none';
        cur.container.style.display = 'block';
        this.active = name;
        if(cur.callback) cur.callback();
    }
    
}

export class listWidget{
    constructor(row_template, callback){
        this.callback = callback;
        this.row_template = row_template;
        this.container = document.createElement('div');
        this.container.className = 'listWidget';
    }
    load(data){
        let cont = this.container;
        while(cont.firstChild) {
            cont.removeChild(cont.firstChild);
        }
        for(let cur of data){
            cont.appendChild(this.helperRow(cur));
        }
    }
    helperRow(data){
        let that = this;
        let tmp = this.row_template.make(data);
        if(this.callback)tmp.addEventListener('click', ()=>{that.callback(data)});
        tmp.addEventListener('mousedown', (e)=>{e.preventDefault()});
        return tmp;
    }
}

export class editorWidget{
    constructor(){
        let toolbar = document.createElement('div');
        toolbar.appendChild(buttonFactory.toolbarHelper(
            ['bold', 'italic', 'underline', 'orderedList', 'unorderedList'], this));
        toolbar.style.display = 'none';
        toolbar.className = 'texttoolbar';
        this.toolbar  = toolbar;
        
        let editarea = document.createElement('div');
        editarea.contentEditable = true;
        editarea.className = 'editarea';
        editarea.textContent='uhmmmm';
        editarea.addEventListener('focus', ()=>{toolbar.style.display='block';});
        editarea.addEventListener('blur', ()=>{toolbar.style.display='none';});
        this.editarea = editarea;
        
        this.container = document.createElement('div');
        this.container.appendChild(toolbar);
        this.container.appendChild(editarea);
        
    }
    load(data){
        this.editarea.innerHTML = data;
    }
    store(){
        return this.editarea.innerHTML;
    }
}

export function headerFactory(leftButton, main, rightButtons){
    let div = document.createElement('div');
    div.className = 'header';
    
    let helper = (className, content)=>{
        let tmp = document.createElement('div');
        tmp.className = className;
        tmp.appendChild(content);
        div.appendChild(tmp);
    };
    
    
    if(typeof main == 'string'){main = document.createTextNode(main);}
    helper('side', leftButton);
    helper('title', main);
    
    if(rightButtons){
        let tmp = document.createDocumentFragment();
        for(let c of rightButtons){tmp.appendChild(c);}
        helper('side', tmp);
    }
    
    return div;
}

export var buttonFactory = {
    makeFromName: (name, callback, className) => {
        let cur = buttonFactory.data[name];
        if(!cur) return;
        if(!className) className = 'button';
        
        if(cur.command) callback = ()=>{document.execCommand(cur.command, null, cur.extraParam)};
        let tmp = document.createElement('img');
        tmp.addEventListener('mousedown', (e)=>{e.preventDefault()});
        tmp.addEventListener('click', callback);
        tmp.alt = cur.label;
        tmp.src = cur.img;
        tmp.className = className;
        return tmp;
    },
    toolbarHelper: (names, bind) => {
        let frag = document.createDocumentFragment();
        for(let cur of names){
            let btn = buttonFactory.makeFromName(cur, this);
            if(!btn) continue;
            frag.appendChild(btn);
        }
        return frag;
    },
    data: {
        // textarea's
        bold:          {label: 'Bold',          img: './images/btn_text_bold.svg',       command: 'bold'},
        italic:        {label: 'Italic',        img: './images/btn_text_italic.svg',     command: 'italic'},
        underline:     {label: 'Underlined',    img: './images/btn_text_underlined.svg', command: 'underline'},
        orderedList:   {label: 'Numbered List', img: './images/btn_list_numbered.svg',   command: 'insertOrderedList'},
        unorderedList: {label: 'Bulleted List', img: './images/btn_list_bulleted.svg',   command: 'insertUnorderedList'},
        
        // app's
        undo:     {label: 'Undo',     img: './images/btn_undo.svg', command: 'undo'},
        redo:     {label: 'Redo',     img: './images/btn_redo.svg', command: 'redo'},
        save:     {label: 'Save',     img: './images/btn_save.svg'},
        load:     {label: 'Load',     img: './images/btn_share.svg'},
        remove:   {label: 'Remove',   img: './images/btn_delete.svg'},
        back:     {label: 'Back',     img: './images/btn_back.svg'},
        settings: {label: 'Settings', img: './images/btn_settings.svg'},
        menu:     {label: 'Menu',     img: './images/btn_menu.svg'},
        label:    {label: 'Label',    img: './images/btn_label.svg'},
        search:   {label: 'Search',   img: './images/btn_search.svg'},
        plus:     {label: 'Plus',     img: './images/btn_plus.svg'},
        
        more:     {label: 'More',     img: './images/btn_more.svg'}
        
    }
    
}
