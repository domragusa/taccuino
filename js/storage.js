export class storage{
    constructor(){
        let req = indexedDB.open('default', 1);
        this.db = new Promise((resolve, reject) => {
            req.onerror   = (e)=> {reject(e.target.errorCode)};
            req.onsuccess = (e)=> {resolve(e.target.result)};
            req.onupgradeneeded = (e)=>{
                let db = e.target.result,
                tmp = db.createObjectStore('notes', {autoIncrement: true});
                tmp.createIndex('lastEdit', 'lastEdit', {unique: false});
                
                db.createObjectStore('labels', {autoIncrement: true});
                tmp = db.createObjectStore('note-labels', {keyPath: ['note', 'label']});
                tmp.createIndex('note', 'note', {unique: false, multiEntry: true});
                tmp.createIndex('label', 'label', {unique: false, multiEntry: true});
            };
        });
    }
    addFile(title, content){
        return this.db.then((db)=>{
            let tx = db.transaction('notes', 'readwrite');
            let req = tx.objectStore('notes').add({title: title, content: content, lastEdit: new Date()});
            
            return helperPromise(req);
        });
    }
    writeFile(id, title, content){
        return this.db.then((db)=>{
            let tx = db.transaction('notes', 'readwrite');
            let data = {title: title, content: content, lastEdit: new Date()};
            
            return helperPromise(tx.objectStore('notes').put(data, id));
        });
    }
    readFile(id){
        return this.db.then((db)=>{
            let tx = db.transaction('notes', 'readonly');
            return helperPromise(tx.objectStore('notes').get(id));
        });
    }
    linkFile(id, label){
        return this.db.then((db)=>{
            let tx = db.transaction('note-labels', 'readwrite');
            return helperPromise(tx.objectStore('note-labels').add({note: id, label: label}));
        });
    }
    
    unlinkFile(id, label){
        return this.db.then((db)=>{
            let tx = db.transaction('note-labels', 'readwrite');
            return helperPromise(tx.objectStore('note-labels').delete([id, label]));
        });
    }
    removeFile(id){
        return this.db.then((db)=>{
            let tx = db.transaction(['notes', 'note-labels'], 'readwrite');
            let idx = tx.objectStore('note-labels').index('note');
            let req = idx.openCursor(IDBKeyRange.only(id));
            
            let del = new Promise((resolve, reject) => {
                req.onsuccess = (e)=>{
                    let cur = e.target.result;
                    if(cur){
                        cur.delete();
                        cur.continue();
                    }else{
                        resolve(id);
                    }
                };
                req.onerror = (e)=>{reject(e.target.errorCode)};
            });
            
            del.then(()=>{
                tx.objectStore('notes').delete(id);
            });
            
            return new Promise((resolve, reject) => {
                tx.oncomplete = resolve;
                tx.onerror    = reject;
            });
        });
    }
    listFiles(){
        return this.db.then((db)=>{
            let tx = db.transaction('notes', 'readonly'),
                idx = tx.objectStore('notes').index('lastEdit'),
                req = idx.openCursor(null, 'prev'),
                data = [];
            
            return new Promise((resolve, reject) => {
                req.onsuccess = (e)=>{
                    let cur = e.target.result;
                    if(cur){
                        data.push({key: cur.key, title: cur.value.title, lastEdit: cur.value.lastEdit});
                        cur.continue();
                    }else{
                        resolve(data);
                    }
                };
                req.onerror = (e)=>{reject(e.target.errorCode)};
            });
        });
    }
    addLabel(name){
        return this.db.then((db)=>{
            let tx = db.transaction('labels', 'readwrite');
            let req = tx.objectStore('labels').add({name: name});
            
            return helperPromise(req);
        });
    }
    renameLabel(id, name){
        return this.db.then((db)=>{
            let tx = db.transaction('labels', 'readwrite');
            let req = tx.objectStore('labels').put({name: name}, id);
            
            return helperPromise(req);
        });
    }
    listLabels(){
        return this.db.then((db)=>{
            let tx = db.transaction('labels', 'readonly'),
                req = tx.objectStore('labels').openCursor(),
                data = [];
            
            return new Promise((resolve, reject) => {
                req.onsuccess = (e)=>{
                    let cur = e.target.result;
                    if(cur){
                        data.push({key: cur.key, name: cur.value.name});
                        cur.continue();
                    }else{
                        resolve(data);
                    }
                };
                req.onerror = (e)=>{reject(e.target.errorCode)};
            });
        });
    }
    getFileLabels(file){
        return this.db.then((db)=>{
            let tx = db.transaction('note-labels', 'readonly');
            let idx = tx.objectStore('note-labels').index('note');
            let req = idx.openCursor(IDBKeyRange.only(file));
            let data = [];
            
            return new Promise((resolve, reject) => {
                req.onsuccess = (e)=>{
                    let cur = e.target.result;
                    if(cur){
                        data.push(cur.value.label);
                        cur.continue();
                    }else{
                        resolve(data);
                    }
                };
                req.onerror = (e)=>{reject(e.target.errorCode)};
            });
        });
    }
    listFilesByLabel(label){
        return this.db.then((db)=>{
            let tx = db.transaction('note-labels', 'readonly');
            let idx = tx.objectStore('note-labels').index('label');
            let req = idx.openCursor(IDBKeyRange.only(label));
            let data = [];
            
            return new Promise((resolve, reject) => {
                req.onsuccess = (e)=>{
                    let cur = e.target.result;
                    if(cur){
                        data.push(cur.value.note);
                        cur.continue();
                    }else{
                        resolve(data);
                    }
                };
                req.onerror = (e)=>{reject(e.target.errorCode)};
            });
        });
    }
    removeLabel(id){
        return this.db.then((db)=>{
            let tx = db.transaction(['labels', 'note-labels'], 'readwrite');
            let idx = tx.objectStore('note-labels').index('label');
            let req = idx.openCursor(IDBKeyRange.only(id));
            
            let del = new Promise((resolve, reject) => {
                req.onsuccess = (e)=>{
                    let cur = e.target.result;
                    if(cur){
                        cur.delete();
                        cur.continue();
                    }else{
                        resolve(id);
                    }
                };
                req.onerror = (e)=>{reject(e.target.errorCode)};
            });
            
            del.then(()=>{
                tx.objectStore('labels').delete(id);
            });
            
            return new Promise((resolve, reject) => {
                tx.oncomplete = resolve;
                tx.onerror    = reject;
            });
        });
    }
}

function helperPromise(request, callback_success, callback_error){
    return new Promise((resolve, reject) => {
        if(!callback_success) callback_success = (e)=>{resolve(e.target.result)};
        if(!callback_error)   callback_error   = (e)=>{reject(e.target.errorCode)};
        
        request.onsuccess = callback_success
        request.onerror   = callback_error;
    });
}
